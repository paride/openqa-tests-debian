# SUSE's openQA tests
#
# Copyright © 2009-2013 Bernhard M. Wiedemann
# Copyright © 2012-2017 SUSE LLC
# Copyright ©      2017 Philip Hands
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

# Summary: Bootloader to setup boot process with arguments/options
# Maintainer: Jozef Pupava <jpupava@suse.com>

use base "debianinstallertest";
use strict;
use testapi;

sub run {
    # wait for the desktop to appear
    assert_screen 'selectLanguage', 300;

    my $language = get_var("LANGUAGE");

    if ("$language" eq "walk_tree") {
        # magic "walk_tree" setting means explore all the language options,
        # then select "C"
        assert_screen 'language_subprompt';
        send_key 'end';

        until (check_screen "NoLocalization", 1) {
            send_key 'ret';
            wait_still_screen 1;
            save_screenshot;
            send_key 'esc';
            unless (check_screen('language_subprompt', 1)) {
                send_key 'down' foreach (1 .. 4);
                wait_still_screen 1;
                save_screenshot;
                send_key 'ret';
                if (check_screen 'kannadaLockup', 1) {
                    sleep 60;
                    die "screen locked up (#987449)" if (check_screen 'kannadaLockup');
                }
                send_key_until_needlematch('language_subprompt', 'esc', 2, 5);
            }
            send_key "up";
            wait_still_screen 1;
            save_screenshot;
        }
        diag "'No Localization' option selected, ending loop";
        send_key 'ret';
        wait_still_screen 2;
        send_key 'ret';
        wait_still_screen 2;
        send_key 'ret';
        send_key_until_needlematch('AmericanEnglish', 'home');
        send_key 'ret';
    }
    elsif (check_var("LANGUAGE", "british")) {    # This is the BritishEnglish branch
        send_key 'ret';                           # Accept English
        start_audiocapture;
        if (check_var("DI_UI", "speech")) {
            assert_screen 'selectLocation';
            sleep 5;
            assert_recorded_sound "speech_selectLocation";
            type_string "15\n";
        }
        else {
            send_key 'up';    # select UK
            assert_screen 'UnitedKingdom';
            send_key 'ret';
        }

        if (check_var("FORCE_US_KEYBOARD", 1)) {
            send_key_until_needlematch('AmericanEnglish', 'home');
        }
        else {
            assert_screen 'BritishEnglish';
            # set this so that `console_loadkeys_us()` knows to revert mapping, to workaround the '"' vs. '@' problem
            set_var('_KEYBOARD_LAYOUT', 'gb');
        }
        send_key 'ret';
    }
    elsif (check_var("LANGUAGE", "georgian")) {
        send_key 'down' foreach (1 .. 6);
        wait_still_screen 1;
        save_screenshot;
        send_key 'ret';

        if (check_screen 'IncompleteTranslation', 5) {
            # Georgian is currently incomplete.
            # If the 'translation-status' file is present (as it is in released
            # versions of the installer), it will warn the user about this.
            assert_screen 'No';
            if (check_var('DI_UI', 'speech')) {
                send_key '1';
            }
            else {
                send_key ('gtk' eq (get_var('DI_UI', 'gtk')) ? 'down' : 'left');
                assert_screen 'Yes';
            }
            send_key 'ret';
        }

        assert_screen 'Georgia';
        send_key 'ret';

        # for now stick with US keys, to avoid the above problem
        send_key_until_needlematch('AmericanEnglish', 'home');

        send_key 'ret';
    }
    else {
        send_key 'ret';
        if (check_var('DISTRI', 'debian-edu')) {
            assert_screen 'selectLocation';
            send_key 'ret';
        }
        assert_screen 'AmericanEnglish';
        send_key 'ret';
    }
}

sub test_flags {
    return {fatal => 1};
}

1;
# vim: set sw=4 et:
