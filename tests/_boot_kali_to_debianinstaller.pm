use base "debianinstallertest";
use strict;
use testapi;
use utils;
use debianinstaller;
use autotest;

sub run {
    assert_screen "kali_boot_menu", 300;
    send_key_until_needlematch "bootmenu_graphicalinstaller", 'down';

    if (get_var('INSTALLONLY')) {
        send_key 'tab';
	wait_still_screen 5;
        type_safely ' debian-installer/exit/poweroff=true';
    }

    send_key 'ret';

    # wait for debianinstaller to appear
    assert_screen "selectLanguage", 150;
    mouse_hide;
}


sub test_flags {
    return { fatal => 1 };
}

1;

# vim: set sw=4 et:
